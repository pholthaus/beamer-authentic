# beamer-authentic

A plain authentic latex beamer theme

## Examples

| Plain style | All features enabled |
| ------ | ------ |
| <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/plain-title.png?job=PDFs" width="400px"> | <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/full-title.png?job=PDFs" width="400px"> |
| <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/plain-items.png?job=PDFs" width="400px"> | <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/full-items.png?job=PDFs" width="400px"> |
| <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/plain-blocks.png?job=PDFs" width="400px"> | <img src="https://gitlab.com/pholthaus/beamer-authentic/-/jobs/artifacts/master/raw/full-blocks.png?job=PDFs" width="400px"> |

## Options

### Title page options

banner image:
```latex
\usebanner[<scale>]{<img>}
%   img:        image to display (filename)             default: <unset>
%   scale:      scale factor (float)                    default: 1
```

logo image for front page and slides:
```latex
\primarylogo[<margin>]{<img>}
%   img:        image to display (filename)             default: <unset>
%   margin:     margin in page header (length)          default: 0pt
```

secondary logo image for front page:
```latex
\secondarylogo{<img>}
%   img:        image to display (filename)             default: <unset>
```

### Slide header options

progress bar below title and subtitle:
```latex
\enableprogress[<style>]{<enabled>}
%   enabled:    enable progress (boolean)               default: true
%   style:      visualization mode (slide or section)   default: slide
```

section listing instead of primary logo in the top right corner:
```latex
\enablesection{<enabled>}
%   enabled:    enable listing                          default: false
```

### Footer options

page counter in the bottom right corner:
```latex
\enablecounter{<enabled>}
%   enabled:    enable page counter                     default: true
```

display e-mail address in the bottom center:
```latex
\myemail{<address>}
%   address:    address to display                     default: <unset>
```

display website in the bottom left corner:
```latex
\myurl{<address>}
%   address:    address to display                     default: <unset>
```